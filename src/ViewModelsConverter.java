import java.util.ArrayList;
import java.util.List;

import to.MaterialTo;
import to.RootModelTo;
import to.TechModelTo;

public class ViewModelsConverter {

    // Сюда приходит список:  TECHNOLOGY,MATERIAL,MATERIAL,..,MATERIAL
    public static TechModelTo toTechModel(List<RowModel> source) {
        TechModelTo result = null;
        for (RowModel rowModel : source) {
            switch (rowModel.positionType) {
                case ROOT -> throw new IllegalArgumentException();
                case TECHNOLOGY -> {
                    if (result != null) {
                        throw new IllegalArgumentException();
                    }
                    result = new TechModelTo(rowModel.anyCode);
                }
                case MATERIAL -> {
                    if (result == null) {
                        throw new IllegalArgumentException();
                    }
                    result.rowTos.add(new MaterialTo(rowModel.anyCode));
                }
            }
        }
        return result;
    }

    // Сюда приходит список:  ROOT,TECHNOLOGY,MATERIAL,MATERIAL,..,MATERIAL, [ROOT,TECHNOLOGY,MATERIAL,MATERIAL,..,MATERIAL], ...
    public static List<RootModelTo> toRootModels(List<RowModel> source) {
        StructureModelBuilder builder = new StructureModelBuilder();
        for (RowModel rowModel : source) {
            switch (rowModel.positionType) {
                case ROOT -> builder.addRoot(rowModel);
                case TECHNOLOGY -> builder.addTech(rowModel);
                case MATERIAL -> builder.addMaterial(rowModel);
            }
        }
        return builder.build();
    }

    private static class StructureModelBuilder {
        private final List<RootModelTo> rootModelTos = new ArrayList<>();
        private List<TechModelTo> techContainer;
        private List<MaterialTo> materialContainer;

        void addRoot(RowModel rowModel) {
            rootModelTos.add(new RootModelTo(rowModel.anyCode));
            techContainer = getActualTechContainer();
            materialContainer = null;
        }

        void addTech(RowModel rowModel) {
            if (techContainer == null){
                throw new IllegalArgumentException();
            }
            techContainer.add(new TechModelTo(rowModel.anyCode));
            materialContainer = getActualMaterialContainer();
        }

        void addMaterial(RowModel rowModel) {
            if (materialContainer == null){
                throw new IllegalArgumentException();
            }
            materialContainer.add(new MaterialTo(rowModel.anyCode));
        }

        List<RootModelTo> build() {
            return rootModelTos;
        }

        private List<TechModelTo> getActualTechContainer() {
            RootModelTo root = rootModelTos.get(rootModelTos.size() - 1);
            if (root.techList == null){
                root.techList = new ArrayList<>();
            }
            return root.techList;
        }

        private List<MaterialTo> getActualMaterialContainer() {
            TechModelTo root = techContainer.get(techContainer.size() - 1);
            if (root.rowTos == null){
                root.rowTos = new ArrayList<>();
            }
            return root.rowTos;
        }
    }
}
